import { Module } from '@nestjs/common';
import { BackpackService } from './backpack.service';
import { BackpackController } from './backpack.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Backpack } from './entities/backpack.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Backpack])],
  controllers: [BackpackController],
  providers: [BackpackService],
})
export class BackpackModule { }

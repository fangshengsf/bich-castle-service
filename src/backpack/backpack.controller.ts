import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { BackpackService } from './backpack.service';
import { CreateBackpackDto } from './dto/create-backpack.dto';
import { UpdateBackpackDto } from './dto/update-backpack.dto';

@Controller('backpack')
export class BackpackController {
  constructor(private readonly backpackService: BackpackService) { }

  @Post()
  create(@Body() createBackpackDto: CreateBackpackDto) {
    return this.backpackService.create(createBackpackDto);
  }
  @Post("/sell")
  sellArticle(@Body() param: { articleId: number, sellNum: number }) {
    return this.backpackService.sellArticle(param);
  }

  @Get()
  findAll() {
    return this.backpackService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.backpackService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateBackpackDto: UpdateBackpackDto) {
    return this.backpackService.update(+id, updateBackpackDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.backpackService.remove(+id);
  }
}

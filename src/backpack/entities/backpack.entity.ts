import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';
@Entity()
export class Backpack {
    @PrimaryGeneratedColumn()
    articleId: number;

    @Column({ type: 'varchar', length: 255, default: '' })
    name: string;

    @Column({ type: 'int', default: 1 })
    num: number;

    @Column({ type: 'int', default: 1 })
    worth: number;

    @Column({ type: 'text' })
    description: string;

    @Column({ type: 'varchar', length: 255, default: '' })
    img: string;

    @Column({ type: 'enum', enum: ['武器', '药水', '道具'] })
    type: string;
}

import { Test, TestingModule } from '@nestjs/testing';
import { BackpackController } from './backpack.controller';
import { BackpackService } from './backpack.service';

describe('BackpackController', () => {
  let controller: BackpackController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [BackpackController],
      providers: [BackpackService],
    }).compile();

    controller = module.get<BackpackController>(BackpackController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});

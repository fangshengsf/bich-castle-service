import { PartialType } from '@nestjs/mapped-types';
import { CreateBackpackDto } from './create-backpack.dto';

export class UpdateBackpackDto extends PartialType(CreateBackpackDto) {}

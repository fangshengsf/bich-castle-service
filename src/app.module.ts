import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UploadModule } from './upload/upload.module';
import { GoodsModule } from './goods/goods.module';
import { ArticleModule } from './article/article.module';
import { MailModule } from './mail/mail.module';
import { BackpackModule } from './backpack/backpack.module';
@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'root',
      password: '123456',
      database: 'db',
      synchronize: true,
      autoLoadEntities: true,
    }),
    UploadModule,
    GoodsModule,
    ArticleModule,
    MailModule,
    BackpackModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }

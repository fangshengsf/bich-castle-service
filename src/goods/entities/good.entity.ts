import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';
import { Length } from 'class-validator';
@Entity()
export class Good {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ type: 'varchar', length: 255, default: '' })
    name: string;

    @Column({ type: 'int', default: 1 })
    price: number;

    @Column({ type: 'int', default: 1 })
    num: number;

    @Length(0, 100, {
        message: '商品描述不能超过100个字符'
    })
    @Column({ type: 'text' })
    description: string;

    @Column({ type: 'varchar', length: 255, default: '' })
    img: string;

    @Column({ type: 'enum', enum: ['武器', '药水', '道具'] })
    type: string;
}

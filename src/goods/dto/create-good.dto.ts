export class CreateGoodDto {
    name: string
    price: number
    description: string
    img: string
    type: string
    num: number
}

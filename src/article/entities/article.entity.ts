import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';
@Entity()
export class Article {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ type: 'varchar', length: 255, default: '' })
    name: string;

    @Column({ type: 'varchar', length: 255, default: '' })
    description: string;

    @Column({ type: 'varchar', length: 255, default: '' })
    img: string;

    @Column({ type: 'int', default: 1 })
    worth: number;

    @Column({ type: 'enum', enum: ['武器', '药水', '道具'], default: '道具' })
    type: string;
}

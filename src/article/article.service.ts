import { Injectable } from '@nestjs/common';
import { CreateArticleDto } from './dto/create-article.dto';
import { UpdateArticleDto } from './dto/update-article.dto';
import { Like, Repository } from 'typeorm';
import { Article } from "../article/entities/article.entity"
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class ArticleService {
  constructor(
    @InjectRepository(Article) private readonly articles: Repository<Article>
  ) { }
  create(createArticleDto: CreateArticleDto) {
    return 'This action adds a new article';
  }
  // 查询所有物品
  async findAll() {
    return await this.articles.find()
  }

  findOne(id: number) {
    return `This action returns a #${id} article`;
  }

  update(id: number, updateArticleDto: UpdateArticleDto) {
    return `This action updates a #${id} article`;
  }

  remove(id: number) {
    return `This action removes a #${id} article`;
  }
}

import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { TransformInterceptor } from "./common/response"
import { HttpExceptionFilter } from "./common/filter"
import { NestExpressApplication } from '@nestjs/platform-express';
import { join } from 'path';
async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);
  // 响应拦截器
  app.useGlobalInterceptors(new TransformInterceptor())
  // 异常过滤器
  app.useGlobalFilters(new HttpExceptionFilter())
  // 静态资源
  app.useStaticAssets(join(__dirname, '..', 'public', 'img'));
  await app.listen(3000);
}
bootstrap();

import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import { Length } from 'class-validator';
import { Prizes } from "./prizes.entity"
@Entity()
export class Mail {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ type: 'varchar', length: 255, default: '' })
    senderName: string;

    @Length(0, 200, {
        message: '内容长度不能超过200字'
    })
    @Column({ type: 'text', nullable: true })
    content: string;

    @Column({ type: 'varchar', length: 255, default: '', nullable: true })
    title: string;

    @Column({ type: 'int', default: '0' })
    mailStatus: number;

    @OneToMany(() => Prizes, (prize) => prize.mail, { cascade: true, onDelete: 'CASCADE' })
    prizes: Prizes[];

    @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
    createdAt: Date;
}


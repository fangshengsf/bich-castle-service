import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';
import { Length } from 'class-validator';
import { Mail } from './mail.entity';
@Entity()
export class Prizes {
    @PrimaryGeneratedColumn({ type: 'int' })
    id: number;

    @Column({ type: 'int' })
    articleId: number;

    @Column({ type: 'varchar', length: 255, default: '' })
    name: string;

    @Column({ type: 'int', default: 1 })
    num: number;

    @Column({ type: 'int', default: 1 })
    worth: number;

    @Length(1, 255, {
        message: '内容长度不能超过255字符'
    })
    @Column({ type: 'text' })
    description: string;

    @Column({ type: 'varchar', length: 255, default: '' })
    img: string;

    @Column({ type: 'enum', enum: ['武器', '药水', '道具'] })
    type: string;

    @ManyToOne(() => Mail, mail => mail.prizes)
    mail: Mail

}


export class CreateMailDto {
    senderName: string;
    content: string;
    title: string;
}

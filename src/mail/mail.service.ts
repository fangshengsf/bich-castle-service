import { Injectable } from '@nestjs/common';
import { CreateMailDto } from './dto/create-mail.dto';
import { UpdateMailDto } from './dto/update-mail.dto';
import { Mail } from "./entities/mail.entity"
import { Prizes } from "./entities/prizes.entity"
import { Backpack } from "../backpack/entities/backpack.entity"
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Prize } from "./types"
@Injectable()
export class MailService {
  constructor(
    @InjectRepository(Mail) private readonly mails: Repository<Mail>,
    @InjectRepository(Prizes) private readonly Prizes: Repository<Prizes>,
    @InjectRepository(Backpack) private readonly backpack: Repository<Backpack>,
  ) { }
  // 添加邮件
  create(createMailDto: CreateMailDto) {
    const data = new Mail();
    data.senderName = createMailDto.senderName;
    data.content = createMailDto.content;
    data.title = createMailDto.title;
    this.mails.save(data);
    return {
      code: 200,
      msg: '添加成功',
    }
  }
  // 添加邮件奖品
  async addPrizes(param: { prizes: Prize[], mailId: number }) {
    const mailInfo = await this.mails.findOne({
      where: {
        id: param.mailId
      },
      relations: ['prizes']
    })
    const prizeList: Prizes[] = []
    for (let i = 0; i < param.prizes.length; i++) {
      const prize = new Prizes();
      prize.articleId = param.prizes[i].id;
      prize.description = param.prizes[i].description;
      prize.img = param.prizes[i].img;
      prize.name = param.prizes[i].name;
      prize.num = param.prizes[i].num;
      prize.type = param.prizes[i].type;
      prize.worth = param.prizes[i].worth;
      await this.Prizes.save(prize);
      prizeList.push(prize)
    }
    mailInfo.prizes = prizeList;
    await this.mails.save(mailInfo);
    return {
      code: 200,
      msg: '添加物品成功',
    }
  }
  // 领取邮件奖品
  async collectPrizes(param: { mailId: number }) {
    const mailInfo = await this.mails.findOne({
      where: {
        id: param.mailId
      },
      relations: ['prizes']
    })
    if (mailInfo.prizes.length === 0) {
      return {
        code: 400,
        msg: '没有物品可以领取'
      }
    } else {
      for (let i = 0; i < mailInfo.prizes.length; i++) {
        const backpackInfo = await this.backpack.findOne({
          where: {
            articleId: mailInfo.prizes[i].articleId,
          }
        })
        if (backpackInfo) {
          backpackInfo.num += mailInfo.prizes[i].num;
          await this.backpack.save(backpackInfo);
        } else {
          const data = new Backpack();
          data.articleId = mailInfo.prizes[i].articleId;
          data.description = mailInfo.prizes[i].description;
          data.img = mailInfo.prizes[i].img;
          data.name = mailInfo.prizes[i].name;
          data.num = mailInfo.prizes[i].num;
          data.type = mailInfo.prizes[i].type;
          data.worth = mailInfo.prizes[i].worth;
          await this.backpack.save(data);
        }
        mailInfo.mailStatus = 1;
        await this.mails.save(mailInfo);
      }
    }
    return {
      code: 200,
      msg: '领取成功'
    }
  }
  // 查询所有邮件
  async findAll() {
    return await this.mails.find({
      relations: ['prizes']
    });
  }

  findOne(id: number) {
    return `This action returns a #${id} mail`;
  }
  // 修改邮件
  update(id: number, updateMailDto: UpdateMailDto) {
    const data = new Mail();
    data.senderName = updateMailDto.senderName;
    data.content = updateMailDto.content;
    data.title = updateMailDto.title;
    data.id = id;
    data.createdAt = new Date();
    this.mails.save(data);
    return {
      code: 200,
      msg: '修改成功',
    }
  }

  // 删除邮件
  async remove(id: number) {
    const mail = await this.mails.find({ relations: ['prizes'], where: { id } });
    if (mail) {
      for (const prize of mail[0].prizes) {
        await this.Prizes.delete(prize.id);
      }
    }
    await this.mails.delete(id);
    return {
      code: 200,
      msg: '删除成功',
    };
  }

}

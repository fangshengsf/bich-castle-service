import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { MailService } from './mail.service';
import { CreateMailDto } from './dto/create-mail.dto';
import { UpdateMailDto } from './dto/update-mail.dto';
import { Prize } from "./types"
@Controller('mail')
export class MailController {
  constructor(private readonly mailService: MailService) { }

  @Post("/add/prizes")
  addPrizes(@Body() param: { prizes: Prize[], mailId: number }) {
    return this.mailService.addPrizes(param);
  }
  @Post("/collectPrizes")
  collectPrizes(@Body() param: { mailId: number }) {
    return this.mailService.collectPrizes(param)
  }
  @Post()
  create(@Body() createMailDto: CreateMailDto) {
    return this.mailService.create(createMailDto);
  }

  @Get()
  findAll() {
    return this.mailService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.mailService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateMailDto: UpdateMailDto) {
    return this.mailService.update(+id, updateMailDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.mailService.remove(+id);
  }
}

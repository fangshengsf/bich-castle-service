export interface Prize {
    id: number,
    name: string,
    description: string,
    img: string,
    type: string,
    num: number,
    worth: number
}
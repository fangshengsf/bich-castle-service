import { Module } from '@nestjs/common';
import { MailService } from './mail.service';
import { MailController } from './mail.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Mail } from './entities/mail.entity';
import { Prizes } from './entities/prizes.entity';
import { Backpack } from "../backpack/entities/backpack.entity"
@Module({
  imports: [TypeOrmModule.forFeature([Mail, Prizes, Backpack])],
  controllers: [MailController],
  providers: [MailService],
})
export class MailModule { }
